import System.Environment
{-# LANGUAGE FlexibleInstances #-}
import Klytius
import Klytius.PGrafo
import Data.Maybe
import Control.Applicative (Applicative ,pure, liftA2, liftA)

suma :: [Int] -> TPar Int
suma [] = mkVars 0
suma [x] = mkVars x
suma (x:xs) = par x' (pseq xs' (x' + xs'))
    where
        x' = mkVars x
        xs' = suma xs

dqsuma :: [Int] -> TPar Int
dqsuma xs = dqsuma' (length xs) xs

dqsuma' :: Int -> [Int] -> TPar Int
dqsuma' _ []  = mkVars 0
dqsuma' _ [x] = mkVars x
dqsuma' 0 xs = error (show xs)
dqsuma' n xs  = par lxs (pseq rxs (lxs + rxs))
    where
        n2 = div n 2
        (lxs',rxs') = (take n2 xs, drop n2 xs)
        (lxs, rxs) = (dqsuma' n2 lxs', dqsuma' (n - n2) rxs')

main = do  
    x : _ <- getArgs
    graficar False (suma $ read x) "sumalista"
    graficar False (dqsuma $ read x) "dqsumalista"
