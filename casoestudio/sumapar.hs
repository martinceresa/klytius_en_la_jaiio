import System.Environment
import Klytius
import Klytius.PGrafo

sumapar :: Int -> Int -> TPar Int
sumapar x y = par x' (pseq y' (x' + y'))
    where
        x' = mkVars x
        y' = mkVars y

main = do
    x' : y' : xs <- getArgs
    let x = read x'
    let y = read y'
    let nombre = "sumapar"++x'++y'
    graficar False (sumapar x y) nombre
    putStrLn ("Archivo generado" ++ nombre)
