import System.Environment
import Klytius
import Klytius.PGrafo

fib :: Int -> TPar Int
fib 0 = mkVars 1
fib 1 = mkVars 1
fib n = pars ("fib("++ (show n) ++")") n1 (pseq n2 (n1 + n2))
    where
        n1 = fib $ n - 1
        n2 = fib $ n - 2

main = do
    x : xs <- getArgs
    let nombre = ("fib."++x)
    graficar False (fib $ read x) nombre
    putStrLn ("Mirar en" ++ nombre)
