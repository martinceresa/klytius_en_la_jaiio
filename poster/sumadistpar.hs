dqsuma :: Int -> [Int] -> TPar Int
dqsuma _ []  = mkVars 0
dqsuma _ [x] = mkVars x
dqsuma n xs  = par lxs (pseq rxs (lxs + rxs))
    where
        n2 = div n 2
        (lxs',rxs') = splitAt n2 xs
        (lxs, rxs) = (dqsuma n2 lxs', dqsuma (n - n2) rxs')
