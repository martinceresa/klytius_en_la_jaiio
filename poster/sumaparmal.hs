suma :: [Int] -> TPar Int
suma [] = mkVars 0
suma [x] = mkVars x
suma (x:xs) = par x' (pseq xs' (x' + xs'))
    where
        x' = mkVars x
        xs' = suma xs
