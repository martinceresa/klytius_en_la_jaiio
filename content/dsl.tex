\section{Lenguajes de Dominio Específico}

Una solución prometedora para independizar un sistema de la forma en
que se va a ejecutar el código consiste en la creación de lenguajes
diseñados exclusivamente para facilitar la implementación de la lógica
de negocios en un dominio en particular. Estos lenguajes son conocidos
como lenguajes de dominio específico (DSL). Un DSL permite trabajar a
un nivel de abstracción ideal, donde los conceptos con los que se
trabaja son los propios del dominio de
aplicación~\parencite{Bentley:1986:PPL:6424.315691}.
%
Por ejemplo, un ingeniero puede escribir un programa en MATLAB u otras
herramientas, utilizando primitivas del lenguaje específicas para el
desarrollo de puentes sin preocuparse por el manejo de estructuras
internas de un lenguaje de programación. Otros ejemplos muy
difundidos de DSLs son \LaTeX{} para la edición de texto científico,
y SQL para el manejo de base de datos.

Los DSL tienen la desventaja de que necesitan del desarrollo de
herramientas especiales.
Mientras que para los lenguajes de propósito general están disponibles,
editores, compiladores (lo que incluye parsers, optimizadores y
generadores de código), bibliotecas, generadores de documentación,
depuradores, etc., los DSL no disponen de herramienta alguna.
Es tarea del desarrollador proveer las herramientas necesarias para
su DSL.
Por esto, han adquirido
popularidad los DSLs embebidos (EDSL) en un lenguaje de propósito
general~\parencite{Hudak:1996:BDE:242224.242477}. Un EDSL es un lenguaje definido dentro de otro
lenguaje, lo que permite utilizar toda la maquinaria ya implementada para el lenguaje anfitrión,
disminuyendo enormemente la tarea al momento de implementar el lenguaje.

Al construir un EDSL, diseñamos un lenguaje con ciertas
primitivas, un cierto \textit{idioma} que nos permite manipular elementos y poder
construir un resultado final en un dominio específico.
Dentro de esta construcción podemos optar por dos
caminos~\parencite{Gill:2014:DLC:2611429.2617811}:
\begin{itemize}
    \item \textbf{Embebido Superficial},
    a medida que el lenguaje va identificando instrucciones, manipula
    los valores sobre los cuales se aplica, retornando un nuevo valor. Es decir,
    no se genera estructura intermedia alguna, sino que simplemente los operadores son funciones
    que van operando sobre los valores directamente. El resultado de los
    lenguajes con embebido superficial es un valor, el resultado de la ejecución de las instrucciones
    dadas.
    \item \textbf{Embebido Profundo},
    utilizando las instrucciones se genera un nuevo tipo abstracto de datos,
    permitiendo al usuario construir un \textit{árbol abstracto} de la computación.
    De esta manera, se puede recorrer la estructura, dando la posibilidad de optimizar
    o sintetizar la computación, y luego ser consumida por una función que evalúa y
    otorga la semántica deseada a los constructores. Se logra separar el idioma del
    EDSL de las operaciones mecánicas que se necesitan para evaluar correctamente el resultado,
    permitiendo, por ejemplo, exportar las computaciones en el caso que se quieran ejecutar
    fuera del entorno de Haskell (como ser, en la GPU) o dar varios
    comportamientos diferentes
    a las operaciones del lenguaje. Permite exponer toda la estructura y composición del programa.
\end{itemize}

{\color{brown}
Para ejemplificar los dos enfoques, supongamos que nuestro idioma está compuesto por
operaciones aritméticas sobre enteros,
más precisamente,
nuestro lenguaje está compuesto por la suma, la resta y la multiplicación.
En el caso del embebido superficial, bastaría con definir un tipo
\haskell{EnteroS} y las siguientes funciones:
\begin{lstlisting}
newtype EnteroS = E Int

suma :: EnteroS -> EnteroS -> EnteroS
suma (E x) (E y) = E (x + y)

resta :: EnteroS -> EnteroS -> EnteroS
resta (E x) (E y) = E (x - y)

mult :: EnteroS -> EnteroS -> EnteroS
mult (E x) (E y) = E (x * y)

runS :: EnteroS -> Int
runS (E x) = x
\end{lstlisting}
Donde, por ejemplo, \haskell{suma (E 4) (E 5) = E 9}.

En el caso de embebido profundo, en cambio, deberíamos definir un nuevo tipo abstracto de datos:

\begin{code}
data EnteroD where
    Lit   :: Int -> EnteroD
    Suma  :: EnteroD -> EnteroD -> EnteroD
    Resta :: EnteroD -> EnteroD -> EnteroD
    Mult  :: EnteroD -> EnteroD -> EnteroD

sumaD :: EnteroD -> EnteroD -> EnteroD
sumaD = Suma
restaD :: EnteroD -> EnteroD -> EnteroD
restaD = Resta
multD :: EnteroD -> EnteroD -> EnteroD
multD = Mult
\end{code}

En el ejemplo antes mencionado, \haskell{sumaD (Lit 4) (Lit 5) = Suma (Lit 4) (Lit 5)}, permitiendo observar directamente la construcción
de la computación.
En el caso que se quiera obtener el resultado de la evaluación, es necesario consumir el árbol abstracto, como se muestra en la
siguiente función:
\begin{lstlisting}
runD :: EnteroD -> Int
runD (Lit n)     = n
runD (Suma x y)  = let
    x' = runD x
    y' = runD y
    in (x' + y')
runD (Resta x y) = let
    x' = runD x
    y' = runD y
    in (x' - y') 
runD (Mult x y)  = let
    x' = runD x
    y' = runD y
    in (x' * y') 
\end{lstlisting}
Utilizando la función \haskell{runD}, se obtiene el resultado de la aplicación de
los operadores que indican los diferentes constructores.
Utilizando dichas definiciones de \haskell{runS} y \haskell{runD}, obtenemos la igualdad
\haskell{runS s = runD d},
para cualquier valor \haskell{s} de tipo \haskell{EnteroS} y \haskell{d} de tipo \haskell{EnteroD}.

Pero en el caso de tener un EDSL profundo, no solo se puede obtener el valor
resultado de evaluar la expresión, sino que, por ejemplo,
se puede contar la cantidad de sumas en una expresión fácilmente sin modificar
las funciones anteriores.
\begin{lstlisting}
contsuma :: EnteroD -> Int
contsuma (Lit _)     = 0
contsuma (Suma l r)  = (contsuma l) + (contsuma r) + 1
contsuma (Resta l r) = (contsuma l) + (contsuma r) 
contsuma (Mult l r)  = (contsuma l) + (contsuma r) 
\end{lstlisting}

De esta manera el embebido profundo combinado con una función específica (como en este caso \haskell{runD}), puede
ser equivalente a un embebido superficial. Además, permite utilizar la construcción de alguna otra manera,
como ser, para extraer más información (\haskell{contsuma}).
}

Los EDSL profundos nos permiten construir el árbol abstracto de las expresiones del lenguaje que describe.
El árbol abstracto contiene la información de cómo un valor dentro del lenguaje fue construido, por lo que
ya no se manipulan valores, sino árboles abstractos. Ésto trae consigo la imposibilidad de utilizar \textbf{directamente}
estructuras de control de flujo, como \emph{if-then-else}, sin consumir la estructura.
Además al tratar con estructuras complejas, y no con valores manipulables
directamente, se reducen las
optimizaciones que el compilador del lenguaje anfitrión puede realizar.
La construcción de ciertos EDSL puede generar ciclos infinitos. Esto quita la posibilidad de 
recorrer la estructura generada, y genera la necesidad de un mecanismo adicional para la detección de dichos ciclos.
