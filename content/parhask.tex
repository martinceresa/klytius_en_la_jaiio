\section{Paralelismo Puro en Haskell}

\subsection{Particionamiento Semi-Explícito.}

Dentro de los lenguajes funcionales se encuentran dos enfoques para paralelismo:
\begin{inparaenum}[\itshape a\upshape)]
\item particionamiento implícito, donde el compilador es el encargado de
        paralelizar los programas;
\item particionamiento explícito, donde el programador es el encargado no
        solo de detectar computaciones a paralelizar, sino que además debe
        proveer mecanismos de organización de computaciones, como ser,
        cómo distribuir las tareas en los procesadores~\parencite{Hammond94parallelfunctional}.
\end{inparaenum}

El lenguaje de programación Haskell presenta un enfoque intermedio, donde el
programador es quien indica cuáles son las computaciones que se pueden
paralelizar, mientras que el \emph{runtime} es el encargado de la coordinación
de las computaciones~\parencite{LoidlTHZB08}. Este enfoque se denomina 
\emph{particionamiento semi-explícito} de programas.
% Es decir,
% se reparten las tareas que dan origen al paralelismo dentro de Haskell,
% entre el programador y el entorno de ejecución en conjunto con el compilador.
% En este enfoque, el programador introduce
% ciertas anotaciones en el programa pero todos los
% aspectos de coordinación son delegados al entorno de ejecución~\parencite{LoidlTHZB08}.
Esta división no es arbitraria, sino que se representan dos niveles de abstracción
diferentes:
\begin{inparaenum}[\itshape a\upshape)]
\item un nivel bajo que depende del hardware de la
        computadora, como ser, cantidad de procesadores, tecnología disponible para la
        comunicación entre los procesadores, donde el compilador y el entorno de
        ejecución son los encargados de utilizar dichos recursos;
\item un nivel alto el cual es especificado por el programador, y es
        inherente al programa, independiente del hardware en el cual se vaya
        a ejecutar o del estado del sistema en el momento de la ejecución. 
\end{inparaenum}
%
% Otros enfoques pueden ser: particionamiento implícito, donde el compilador es el
% encargado de detectar el paralelismo dentro de un programa; particionamiento
% explícito, donde el programador es el encargado de no solo seleccionar las
% computaciones a paralelizar, sino que deberá establecer mecanismos de
% distribución de tareas en los procesadores, de comunicación de procesos, etc~\parencite{Hammond94parallelfunctional}.
%
\subsection{Primitivas Básicas de Paralelismo.}

El lenguaje de programación Haskell, cuenta con dos combinadores básicos de coordinación de computaciones.
\begin{lstlisting}
    par,pseq  :: a → b → b
\end{lstlisting}
Ambos combinadores son los únicos con acceso al control del paralelismo puro del programa\footnote{Si bien existen otros
métodos, en general no se recomienda mezclar varias técnicas.}. 

Denotacionalmente ambas primitivas son proyecciones en su segundo argumento.
\begin{lstlisting}
par  a b = b
pseq a b = b
\end{lstlisting}
Operacionalmente \lstinline{pseq} establece que el primer argumento
\textbf{debe} ser evaluado antes que el segundo, mientras que \lstinline{par}
indica que el primer argumento \textbf{puede} llegar a ser evaluado en paralelo
con la evaluación del segundo. Utilizando los combinadores básicos \haskell{par}
y \haskell{pseq} el programador establece el \textbf{qué} y \textbf{cómo}
paralelizar sus programas, dejando la coordinación de computaciones al
compilador en conjunto con el \emph{runtime}.

Definiremos como \emph{comportamiento dinámico} a los efectos generados por el
uso de los combinadores básicos de paralelismo, los cuales son reflejados en
tiempo de ejecución. 

Toda computación \haskell{a} marcada por \haskell{par a b}, será vista por el
entorno de ejecución como una oportunidad de trabajo a realizar en paralelo, la
cual se denomina \emph{spark}.  La acción de \emph{sparking} no fuerza
inmediatamente la creación de un \emph{hilo}, sino que es el \emph{runtime} el
encargado de determinar cuáles \emph{sparks} van a ejecutarse, tomando esta
decisión en base al estado general del sistema, como ser, si encuentra
disponible a algún núcleo en el cual ejecutar la evaluación del \emph{spark}.
El \emph{runtime} es también el encargado de manejar los detalles de la
ejecución de computaciones en paralelo, como ser, la creación de hilos, la
comunicación entre ellos, el \emph{workload balancing}, etc.

Los combinadores básicos \haskell{par} y \haskell{pseq}, nos permiten expresar
paralelismo de manera fácil, pero no siempre de manera correcta. Por ejemplo en
el fragmento de Código~\ref{lst:parsuma:par} se muestra cómo paralelizar la
evaluación de un entero (\haskell{x}), con la evaluación del resultado de la
expresión (\haskell{x+y}). Pero el programa mostrado en el fragmento de
Código~\ref{lst:parsuma:par}, presenta un error sutil.  La función \haskell{(+)}
tal como se encuentra definida en el \emph{preludio} de Haskell, es estricta en
su primer argumento, por lo que evaluará primero a \haskell{x}. Es decir que el
valor del \emph{spark} generado por el combinador \haskell{par} (para la
evaluación de \haskell{x}) será necesitado inmediatamente al evaluar
(\haskell{(+) x y}), \textbf{eliminando} toda posibilidad de paralelismo.  La
forma correcta de definir una función suma que evalúe sus argumentos en paralelo
y luego realice la suma de sus valores es la que se muestra en el fragmento de
Código~\ref{lst:parsuma}, donde se utiliza el combinador \haskell{pseq} para
forzar la evaluación de \haskell{y}, dejando la posibilidad que el \emph{spark}
destinado a la evaluación de \haskell{x} pueda realizar su trabajo.


\begin{program}
\begin{lstlisting}
parsuma :: Int -> Int -> Int
parsuma x y = par x (x + y)
\end{lstlisting}
\caption{Suma de enteros utilizando \haskell{par}.}
\label{lst:parsuma:par}
\end{program}

\begin{program}
\begin{lstlisting}
parsuma :: Int -> Int -> Int
parsuma x y = par x (pseq y (x + y))
\end{lstlisting}
\caption{Suma de enteros paralelos.}
\label{lst:parsuma}
\end{program}

\subsection{Observando Paralelismo}

Utilizar los combinadores básicos de paralelismo resulta sencillo, pero no nos
otorga ninguna garantía que los programas se ejecuten efectivamente de forma
paralela. Por ejemplo, en el caso de sumar dos enteros (ver
Código~\ref{lst:parsuma:par}) se elimina el paralelismo debido a que el operador
\haskell{(+)} es estricto en su primer argumento. 

Para estudiar de forma más profunda el paralelismo dentro de Haskell, ya sea
al momento de diseñar un programa, o al experimentar nuevas abstracciones de
paralelismo, es útil contar con una estructura visible del comportamiento de
nuestros programas.

Proponemos la utilización de la herramienta \emph{Klytius} la cual permite
observar directamente la estructura dinámica de los programas, e incluye la
posibilidad de representar gráficamente dicha estructura. Por ejemplo, para el
caso de la suma paralela de enteros (ver Código~\ref{lst:parsuma}) podemos
representarlo como se muestra en la Figura~\ref{img:parsuma}, donde la elipse
representa al combinador básico \haskell{par}, del cual se desprenden dos nodos,
con la línea segmentada marcamos la computación que es evaluada en paralelo,
mientras que con la línea continua marcamos la computación resultante. El
hexágono representa al combinador \haskell{pseq}, y con una línea punteada la
computación que es evaluada primero, y con una linea continua la computación
resultante. Los valores se representan dentro de cajas.

\begin{figure}
        \centering
        \includegraphics[scale=0.8]{parsuma.pdf}
        \caption{Representación gráfica de \haskell{parsuma 4 7}.}
        \label{img:parsuma}
\end{figure}

Para representar un programa dentro de \emph{Klytius}, se deben realizar
pequeñas modificaciones al código de la función.  A continuación, se puede
observar a modo de comparación los dos códigos de la función \haskell{parsuma},
a izquierda utilizando la librería \haskell{Control.Parallel} y a derecha a
\emph{Klytius}.
\\
\begin{minipage}[t]{.5\textwidth}
        \begin{lstlisting}
parsuma :: Int -> Int -> Int
parsuma x y =
    par x (pseq y (x + y))
        \end{lstlisting}
\end{minipage}
\begin{minipage}[t]{.6\textwidth}
        \begin{lstlisting}
parsuma :: Int -> Int -> TPar Int
parsuma x y =
    par x' (pseq y' (x' + y'))
    where
        x' = mkVars x
        y' = mkVars y
        \end{lstlisting}
\end{minipage}

El constructor de tipo \haskell{TPar}, nos permite indicar que estamos ante la
presencia de cierto comportamiento dinámico adicional, el cual se encuentra
encapsulado dentro de \haskell{TPar}.  La función \haskell{mkVars} de tipo
\haskell{a -> TPar a} nos permite insertar elementos dentro del tipo
\haskell{TPar}, sin comportamiento dinámico adicional, pero con la capacidad de
adquirirlo.

\subsubsection{Paralelizar la suma de enteros.}

Un ejemplo un poco más complejo es sumar una lista de enteros. Mostraremos dos
aproximaciones a la solución.

Una primera aproximación es pensar en paralelizar la evaluación del elemento a
la cabeza de la lista, con la evaluación de una llamada recursiva sobre el resto
de la lista, para luego sumar los resultados. De forma similar al ejemplo
anterior, se puede implementar como se muestra en el fragmento de
Código~\ref{lst:sumal}.

\begin{program}
        \begin{lstlisting}
sumal :: [Int] -> TPar Int
sumal [] = mkVars 0
sumal [x] = mkVars x
sumal (x:xs) = par x' (pseq xs' (x' + xs'))
    where
        x' = mkVars x
        xs' = sumal xs
        \end{lstlisting}
        \caption{Función \haskell{sumal}.}
        \label{lst:sumal}
\end{program}

\begin{figure}
        \centering
        \includegraphics[scale=0.5]{sumalista.pdf}
        \caption{Representación gráfica de \haskell{sumal [3,4,5,6]}}
        \label{img:sumal}
\end{figure}

Podemos obtener una representación gráfica que se muestra en la
Figura~\ref{img:sumal}, donde se observa una repetición del comportamiento
dinámico al momento de realizar un \emph{spark} de las computaciones, por
ejemplo, el gráfico comienza con una elipse, y continúa a un hexágono, para
luego bifurcarse en dos computaciones que aparentemente tienen la misma estructura.  Esto se
debe a que no es exactamente la misma estructura, sino que el valor resultante
es diferente, y se puede diferenciar mediante la etiqueta de la elipse,
\rhaskell{(+) 3}.  El patrón se origina en la expresión \haskell{pseq xs'
(x'+xs')}, donde la suma de dos valores con comportamiento dinámico, es evaluar
el comportamiento dinámico de \haskell{x'}, luego el de \haskell{xs'}, y por
ultimo realizar la suma de los valores que representan. Como \haskell{x'} no
posee comportamiento dinámico, sólo observamos el comportamiento de
\haskell{xs'}. Esto da origen a la repetición de estructuras en el gráfico, pero
aquí se observa que en realidad contienen diferentes valores, en \haskell{xs'}
representa el resultado de sumar la \emph{cola} de la lista, mientras que en
\haskell{x'+xs'} representa el resultado de sumar toda la lista, aunque ambas
contienen el mismo comportamiento dinámico.

Podemos observar a simple vista el efecto de las computaciones paralelas, el
cual evalúa los valores que comparten con el resto de las computaciones,
como ser, la evaluación de las expresiones dentro de las cajas. 

% Esta bifurcación de caminos, da una pauta de las posibles formas que puede
% tomar la ejecución al momento de evaluar las diferentes expresiones, por ejemplo,
% en el caso de que haya un sólo procesador las
% computaciones en paralelo serán ignoradas, es decir, se ignorarán las flechas
% segmentadas.

% Al observar la estructura que se muestra en la
% Figura~\ref{img:sumal}, se encuentra un patrón de un hexágono que bifurca en dos
% caminos similares, particularmente la computación que se realiza primero evalúa
% los valores necesarios, para que luego sean sumados.
% Esta estructura se origina
% por la forma en que la función \haskell{sumal} fue implementada (ver
% Código~\ref{lst:sumal}). Particularmente en la linea:
% \begin{lstlisting}
% sumal (x:xs) = par x' (pseq xs' (x'+xs'))
% \end{lstlisting}
% El patrón se origina en la expresión \haskell{pseq xs' (x'+xs')}, donde la suma
% de dos valores con comportamiento dinámico, es evaluar el comportamiento
% dinámico de \haskell{x'}, luego el de \haskell{xs'}, y por ultimo realizar la
% suma de los valores que representan. Como \haskell{x'} no posee
% comportamiento dinámico, observamos el comportamiento de \haskell{xs'}. Esto da
% origen a la repetición de estructuras en el gráfico, pero aquí se observa que
% en realidad contienen diferentes valores, en \haskell{xs'} representa el
% resultado de sumar la \emph{cola} de la lista, mientras que en \haskell{x'+xs'}
% representa el resultado de sumar toda la lista, aunque ambas contienen el mismo
% comportamiento dinámico.

Las etiquetas del gráfico en conjunto con la gran cantidad de \haskell{pseq} que
se observan, nos indican que estamos ante la presencia de un proceso secuencial.
De la manera en que implementamos \haskell{sumal}, utilizamos el
paralelismo para evaluar las expresiones de los enteros que tomamos como
argumento, pero \textbf{no} paralelizamos la aplicación de la suma. Dada una
lista \haskell{[x1, x2, ..., xn]}, \haskell{sumal [x1, x2, ..., xn]} evalúa las
expresiones \haskell{x1}, \haskell{x2}, ..., \haskell{xn} en paralelo, pero
devuelve el resultado \haskell{x1 + (x2 + ... (xn-1 + (xn)) ...)}. Si seguimos
las etiquetas de las elipses, podemos observar de forma simple cuando van
ocurriendo estas sumas.

Para explotar todo el paralelismo de nuestro algoritmo podemos
dividir la lista en dos, evaluar las llamadas recursivas en paralelo, y
luego sumarlas. Una forma de implementar ésta idea es como se muestra en el
fragmento de Código~\ref{lst:dqsuma}.

\begin{program}
        \begin{lstlisting}
dqsuma :: [Int] -> TPar Int
dqsuma xs = dqsuma' (length xs) xs

dqsuma' :: Int -> [Int] -> TPar Int
dqsuma' _ []  = mkVars 0
dqsuma' _ [x] = mkVars x
dqsuma' 0 xs = error (show xs)
dqsuma' n xs  = par lxs (pseq rxs (lxs + rxs))
    where
        n2 = div n 2
        (lxs',rxs') = splitAt n2 xs
        (lxs, rxs) = (dqsuma' n2 lxs', dqsuma' (n - n2) rxs')
        \end{lstlisting}
        \caption{Algoritmo de suma de lista de enteros estilo divide \& conquer}
        \label{lst:dqsuma}
\end{program}

Podemos observar gráficamente la estructura dinámica del algoritmo
\haskell{dqsuma} en la Figura~\ref{img:dqsuma}, donde se ve directamente cómo la
lista es divida en dos partes, y mientras un \emph{spark} da origen a la
evaluación de la primer mitad (\haskell{[3,4]}), se fuerza la evaluación del
\emph{spark} destinado a evaluar la segunda mitad (\haskell{[5,6]}).

\begin{figure}
        \centering
        \includegraphics[scale=0.5]{dqsumalista.pdf}
        \caption{Representación gráfica de \haskell{dqsuma [3,4,5,6]}}
        \label{img:dqsuma}
\end{figure}
