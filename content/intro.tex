\section{Introducción}
La presencia de procesadores multinúcleo en la mayoría de los dispositivos
condiciona al desarrollador a escalar sus programas mediante la ejecución
paralela.  Debido a la necesidad de incorporar paralelismo en los programas se
han extendido los lenguajes de programación, o bien se ha desarrollado
bibliotecas, con diversos grados de éxito. El estudio de las formas de
incorporar de manera simple y eficiente la programación paralela al desarrollo
de software sigue siendo objeto de investigación. En particular, en el lenguaje
de programación Haskell~\parencite{Marlow_haskell2010} se ha desarrollado una
variedad de extensiones, bibliotecas, y abstracciones, que permiten encarar el
problema de la programación paralela desde diversos ángulos.

El desarrollo de herramientas adecuadas que permitan el análisis de las
ejecuciones obtenidas es necesario tanto al momento de dar soporte a la
experimentación con nuevas formas de incorporar paralelismo, introducir nuevos
estudiantes a la programación paralela, o para comprender el comportamiento de
los programas.

En este trabajo se le otorga al programador la posibilidad de observar directamente
qué computaciones se van a paralelizar, permitiéndole tener un mayor
entendimiento sobre la ejecución de su programa. Se presenta el desarrollo de la
herramienta \emph{Klytius}, donde se implementa un lenguaje de dominio
específico destinado a representar programas paralelos, y se definen
representaciones gráficas para la estructura de programas paralelos.

El artículo se encuentra estructurado de la siguiente forma:
\begin{inparaenum}[]
    \item sección 1, introducción al paralelismo y sus problemáticas;
    \item sección 2, introducción al paralelismo básico en Haskell y a Klytius;
    \item sección 3, implementación de Klytius;
    \item sección 4, trabajo relacionado;
    \item sección 5, conclusiones y trabajo futuro.
\end{inparaenum}
% \todo[inline]{El trabajo se encuentra estructurado de la siguiente forma...
% Sección 1 introduccion al paralelismo de forma general. Sección 2,introducción
% al paralelismo básico de Haskell y Klytius. Sección 3, DSL. Sección 4
% implementación de Klytius.}

\subsection{Problemáticas del Paralelismo}

Para explotar todo el hardware subyacente el programador debe
\textit{pensar} de manera paralela, obteniendo beneficios en velocidad,
con la desventaja que el diseño del software se torna más complicado.
Se debe tener en cuenta dos aspectos: 
\begin{inparaenum}
    \item intrínseco al diseño del algoritmo, identificar qué tareas
    se pueden realizar en paralelo, analizar la dependencia de datos, etc;
    \item configuración del hardware disponible, cantidad de núcleos disponibles,
    cómo distribuir las diferentes tareas y establecer canales
    de comunicación entre ellos.
\end{inparaenum}

Debido a limitaciones físicas no es posible continuar aumentando la velocidad de
procesamiento de los microprocesadores. Esto produce que el programador deba
incorporar el paralelismo dentro del diseño de sus programas, o modificar sus
algoritmos.

El programador debe dedicar tiempo y esfuerzo en pensar cómo paralelizar sus
programas, y en el caso de que sean modificados, garantizar que no se introducen
nuevos errores.

% Mientras que, cuando se aumentaba la frecuencia de los
% microprocesadores se ejecutaban exactamente las mismas instrucciones más rápido.

% Cuando se aumenta la frecuencia de los microprocesadores se ejecutan
% \textbf{exactamente las mismas} instrucciones más rápido.
% Es decir, se ejecutan con mayor velocidad \textbf{los mismos algoritmos} sin modificación
% alguna. Dejando al programador simplemente la tarea de
% conseguir hardware más potente para que los programas funcionen más rápido,
% librándolo de modificar su código o pensar en cómo administrar las computaciones
% de su programa.
%
Al aumentar las unidades de procesamiento se da lugar a la posibilidad de ejecutar
varias instrucciones de manera simultánea, es decir, permite \textit{paralelizar}
la \textbf{ejecución} de los programas. Esto, en teoría, permite multiplicar el poder de cómputo,
aunque, en la práctica no es tan fácil obtener un mayor rendimiento.
Esto sucede principalmente por dos razones:
\begin{itemize}
    \item No todo es paralelizable. Hay tareas que son inherentemente secuenciales, es decir, que no se van
    a poder paralelizar completamente sino que tienen al menos un proceso el cual se tiene que
    realizar de manera secuencial.
    \item Paralelizar no es \emph{gratis}. Aún dados algoritmos que son totalmente paralelizables, 
    deberemos saber cómo dividiremos los datos, cuantos núcleos hay disponibles,
    cómo distribuiremos las distintas
    tareas en los diferentes núcleos y cómo se comunican entre ellos 
    en el caso que sea necesario.
\end{itemize}
%
% Como resultado, el programador debe modificar su código para incorporar la
% posibilidad de ejecutar su programa utilizando todos los núcleos disponibles.

\subsection{Paralelismo en Lenguajes Funcionales Puros} 

Los lenguajes funcionales puros nos permiten representar las computaciones en términos
de funciones puras, es decir, nos permiten presentar a un programa como una función
que obtiene toda la información necesaria a través de sus argumentos y devuelve algún
valor como resultado de su computación, sin utilizar información externa
a ella que no este explícitamente dentro de sus argumentos, ni modificar nada
fuera del resultado que otorga al finalizar. Por lo tanto todo comportamiento
se vuelve explícito. Esto permite que sea más fácil razonar sobre los programas
y estudiar su comportamiento.

Particularmente al utilizar lenguajes funcionales puros obtenemos las siguientes
ventajas~\parencite{Roe91parallelprogramming}:
\begin{itemize}
        \item Nos permite razonar sobre los programas paralelos de la misma
                manera que los secuenciales.
        \item Nos permite delegar todos los aspectos de coordinación de
                computaciones al \emph{runtime}.
        \item No existe la posibilidad de que haya \emph{deadlock}, excepto en
                condiciones donde su versión secuencial no termine debido a
                dependencias cíclicas.
\end{itemize}
% \todo[inline]{donde pongo la referencia? Esto es de roe}

%Determinism
% Particularmente al tener computaciones puras la ejecución se vuelve determinista,
% el resultado \textbf{sólo} depende de sus argumentos. Por lo tanto, todo programa que se ejecute
% secuencialmente tendrá el mismo resultado que su versión paralela con los mismos datos
% de entrada, resaltando que el paralelismo es una herramienta para arribar a la solución más rápido.
% Esto nos permite razonar sobre los programas paralelos de la misma manera
% que los secuenciales y depurarlos sin la necesidad de efectivamente ejecutarlos en paralelo.
% No existe la posibilidad
% de que haya \textit{deadlock}, excepto en condiciones donde su versión secuencial
% no termine debido a dependencias cíclicas~\parencite{Roe91parallelprogramming}.

%Good for parallel V2.
% Los lenguajes funcionales, nos permiten delegar todos los aspectos de coordinación al 
% \textit{runtime}, es decir, libran al programador de tareas como:
% distribuir las diferentes tareas entre los núcleos, establecer canales de comunicación
% entre los procesos, la generación
% de nuevas tareas e incluso es independiente tanto de la arquitectura
% como de las políticas de \textit{scheduling}.
% Esto se debe a que los lenguajes funcionales utilizan como
% mecanismo de ejecución abstracto la \textit{Reducción de Grafos}
% \parencite{Johnsson:1984:ECL:502949.502880,Augustsson:1984:CLM:800055.802038,Kieburtz:1985:GFG:5280.5305}
% el cual es fácil de modificar para permitir la evaluación paralela de las computaciones
% \parencite{Augustsson:1989:PGR:99370.99386}. Estos sistemas representan a las computaciones
% como grafos donde cada hoja es un valor y los nodos, que no son hojas, representan aplicaciones.
% Se basan en ir reduciendo cada nodo a su valor correspondiente, posiblemente generando
% que otros subgrafos sean evaluados en el proceso.

% { \color{brown}
% En particular, Haskell es un lenguaje funcional puro con evaluación \textit{perezosa}.
% Las computaciones son evaluadas en el momento que su valor sea necesario o no
% evaluadas si no lo son. Al momento de incorporar paralelismo se requiere
% otorgar la posibilidad de evaluar expresiones sobre los distintos núcleos disponibles,
% posiblemente sin que esas expresiones se necesiten en ese momento, por lo que será
% necesario prestar atención cuando se trata de paralelizar una computación.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Se deberá forzar explícitamente la evaluación que se
% requiera para una expresión dada, ya que sino se suspenderá su evaluación
% y sólo se evaluará (de manera secuencial) cuando se necesite su valor.
% }


\subsection{Contribuciones}

Hemos desarrollado un lenguaje de dominio específico embebido en Haskell para la
simulación simbólica de programas paralelos. A su vez, hemos asignado una
representación gráfica a las diferentes construcciones del lenguaje de dominio
específico, que en conjunto permite al programador observar directamente qué
computaciones se van a paralelizar, dándole un mayor entendimiento sobre la
ejecución de su programa. 

% Aportes:
% \begin{itemize}
%         \item Implementación de \emph{Klytius}
%         \item Representación gráficas de programas paralelos
% \end{itemize}

% En este trabajo se desarrolla un lenguaje de dominio específico
% embebido en Haskell que permite la simulación simbólica de programas
% paralelos. Esta permite al programador observar
% directamente qué computaciones se van a paralelizar, dándole
% un mayor entendimiento sobre la ejecución de su programa. Además, se presenta
% el desarrollo de la herramienta \emph{Klytius}, donde se implementa el lenguaje de
% dominio específico destinado a representar programas paralelos.
% Éste lenguaje
% provee un conjunto de operadores que permiten construir programas paralelos
% simplemente, utilizando operadores similares a los
% combinadores de paralelismo puro dentro de Haskell, 
% pero a la vez otorgando la posibilidad de realizar gráficos
% que representan la evaluación de las computaciones.

Actualmente la herramienta se encuentra alojada en un repositorio \emph{Git}
público, \url{https://bitbucket.org/martinceresa/klytius/}. Se presenta como un
paquete a instalar por la herramienta \emph{Cabal} de la plataforma de Haskell.

El presente artículo es una versión reducida del trabajo de finalización de la carrera
de grado de Licenciatura en Ciencias de la Computación. El trabajo fue realizado
bajo la supervisión de Mauro Jakelioff y Ezequiel Rivas, a los cuales estoy muy
agradecido por su dedicación.
