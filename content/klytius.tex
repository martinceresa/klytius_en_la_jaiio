\section{Klytius}

\subsection{Lenguajes de Dominio Específico}

Una solución prometedora para independizar un sistema de la forma en
que se va a ejecutar el código consiste en la creación de lenguajes
diseñados exclusivamente para facilitar la implementación de la lógica
de negocios en un dominio en particular. Estos lenguajes son conocidos
como lenguajes de dominio específico (DSL). Un DSL permite trabajar a
un nivel de abstracción ideal, donde los conceptos con los que se
trabaja son los propios del dominio de
aplicación~\parencite{Bentley:1986:PPL:6424.315691}.

Los DSL tienen la desventaja de que necesitan del desarrollo de
herramientas especiales, como ser, editores, compiladores, etc.
Es tarea del desarrollador proveer las herramientas necesarias para
su DSL. Por esto, han adquirido
popularidad los DSLs embebidos (EDSL) en un lenguaje de propósito
general~\parencite{Hudak:1996:BDE:242224.242477}. Un EDSL es un lenguaje definido dentro de otro
lenguaje, lo que permite utilizar toda la maquinaria ya implementada para el lenguaje anfitrión,
disminuyendo enormemente la tarea al momento de implementar el lenguaje.

Al construir un EDSL, diseñamos un lenguaje con ciertas
primitivas, un cierto \textit{idioma} que nos permite manipular elementos y poder
construir un resultado final en un dominio específico.
Dentro de esta construcción podemos optar por dos
caminos~\parencite{Gill:2014:DLC:2611429.2617811}:
\begin{itemize}
    \item \textbf{Embebido Superficial},
    a medida que el lenguaje va identificando instrucciones, manipula los
    valores sobre los cuales se aplica, retornando un nuevo valor. Es decir, no
    se genera estructura intermedia alguna, sino que simplemente los operadores
    son funciones que van operando sobre los valores directamente. Al operar
    directamente sobre los valores obtenemos como resultado el valor de la
    ejecución de las instrucciones dadas.
    \item \textbf{Embebido Profundo},
    utilizando las instrucciones se genera un nuevo tipo abstracto de datos,
    permitiendo al usuario construir un \textit{árbol abstracto} de la
    computación.  De esta manera, se puede recorrer este árbol, dando la
    posibilidad de optimizar o sintetizar la computación, y luego ser consumida
    por una función que evalúa y otorga la semántica deseada a los
    constructores. Se logra separar el idioma del EDSL de las operaciones
    mecánicas que se necesitan para evaluar correctamente el resultado,
    permitiendo, por ejemplo, exportar las computaciones en el caso que se
    quieran ejecutar fuera del entorno de Haskell (como ser, en la GPU) o dar
    varios comportamientos diferentes a las operaciones del lenguaje. Permite
    exponer toda la estructura y composición del programa.
\end{itemize}

Los EDSL profundos nos permiten construir el árbol abstracto de las expresiones
del lenguaje que describe. El árbol abstracto contiene la información de cómo
un valor dentro del lenguaje fue construido, lo que permite analizar dicha
información de diferentes maneras. Así mismo al tener el árbol abstracto ya no
se manipulan valores. Ésto trae consigo la imposibilidad de utilizar
\textbf{directamente} estructuras de control de flujo, como \emph{if-then-else},
sin consumir la estructura.  Además al tratar con estructuras complejas, y no
con valores manipulables directamente, se reducen las optimizaciones que el
compilador del lenguaje anfitrión puede realizar.  La construcción de ciertos
EDSL puede generar ciclos infinitos. Esto quita la posibilidad de recorrer la
estructura generada, y genera la necesidad de un mecanismo adicional para la
detección de dichos ciclos.

\subsection{EDSL profundo para Paralelismo en Haskell}

En Haskell hay una gran diversidad de abstracciones y DSLs para
modelar paralelismo. Entre los más populares están, la mónada
Eval~\parencite{EvalMonad}, la mónada Par~\parencite{ParMonad},
Data Parallel Haskell~\parencite{DPH}, la librería
Repa~\parencite{REPA}, el lenguaje Accelerate~\parencite{Accelerate},
y el lenguaje Obsidian~\parencite{Obsidian}. Una comparación entre
varios enfoques puede verse en~\parencite{ParallelHakells}.

Dado que nuestro interés está centrado en el estudio del paralelismo esencial dentro de Haskell, desarrollamos
un EDSL profundo en base a los combinadores básicos, \haskell{par} y \haskell{pseq}.
Tomando estos combinadores como constructores del
lenguaje, definimos un tipo de datos como se muestra en el fragmento de
Código~\ref{lst:edsl:cap4}.

\begin{program}
\begin{lstlisting}
data TPar s where
    Par :: TPar a -> TPar s -> TPar s
    Seq :: TPar a -> TPar s -> TPar s
    Val :: s -> TPar s
\end{lstlisting}
\caption{EDSL profundo para modelar paralelismo.}
\label{lst:edsl:cap4}
\end{program}

Este tipo de datos nos permite modelar el comportamiento dinámico de las
computaciones generadas al momento de ejecutar el programa. Es, además, un tipo
de datos con estructura de árbol, donde las hojas son los valores introducidos
por el constructor~\haskell{Val} y los nodos (internos) representan el
combinador \haskell{par}~(\haskell{Par}), y el combinador
\haskell{pseq}~(\haskell{Seq}).

Cuando en Haskell utilizamos los operadores básicos \haskell{par} y
\haskell{pseq}, el resultado es un comportamiento dinámico que no es observado
directamente, sino que, como ya se ha mencionado, \haskell{par x y = y} y
\haskell{pseq x y = y}. Utilizando el EDSL planteado en \nombre{} se puede
observar la construcción del comportamiento dinámico de la expresión, ya que se
conserva toda la información de los operadores básicos utilizados, es decir,
\haskell{par x' y' = Par x' y'} y \haskell{pseq x' y' = Seq x' y'}, donde tanto
\haskell{x'}, como \haskell{y'}, pueden tener a su vez comportamiento dinámico
adicional.
 
\begin{program}
    \begin{lstlisting}
parsuma :: Int -> Int -> TPar Int
parsuma l r =
    par l' (pseq r' (l' + r'))
    where
       l' = mkVar l
       r' = mkVar r
    \end{lstlisting}
    \caption{Suma de enteros paralela}
    \label{lst:parsuma:cap4}
\end{program}

Por ejemplo, para la suma paralela (ver Código~\ref{lst:parsuma:cap4}), obtenemos el siguiente árbol abstracto:
\begin{lstlisting}
parsuma 4 7 = Par (Val 4) (Seq (Val 7) (Val ((+) 4 7) ))
\end{lstlisting}

De esta manera es posible construir el árbol abstracto de las construcciones
dinámicas de nuestro programa, que nos permite \textbf{observar todo el
comportamiento dinámico de la computación}.  El EDSL de \nombre{} provee
primitivas de alto nivel para construir fácilmente dicho \textit{AST}, y luego,
mostrarlo gráficamente, permitiéndole al programador realizar un análisis más
exhaustivo del programa.

\subsection{Observando Computaciones Compartidas}

Una parte vital del desarrollo de la herramienta es la detección de computaciones compartidas.
Éste es un problema muy estudiado
dentro de los EDSL y en optimizaciones de compiladores, dado que las computaciones compartidas
permiten evaluar una única
vez una expresión y luego compartir el resultado, minimizando el trabajo a realizar.
Por ejemplo, en una expresión como \haskell{doble x = let y = x + 1 in y + y},
la expresión \haskell{y} es compartida para la suma (\haskell{y+y}), y su evaluación puede
realizarse una sola vez.

% Al desarrollar un programa paralelo en Haskell, el programador, genera
% \emph{sparks} para la evaluación de expresiones que se podrían utilizar luego.
% Es decir, cuando tenemos una
% expresión como:
% \begin{lstlisting}
%     par x m \end{lstlisting}
% la computación \haskell{x} es luego utilizada en \haskell{m}.
% Esto mismo, podemos verlo directamente en una expresión como \haskell{par y y}. Nuestro objetivo
% es representar las computaciones de manera gráfica, por lo que deberíamos identificar en este caso a
% \haskell{y} como una referencia a la misma computación, así como identificar
% todas las referencias de \haskell{x} que ocurran dentro de \haskell{m}.

En un EDSL profundo las computaciones son representadas como un árbol abstracto, por lo que las computaciones
recursivas o que poseen referencias mutuas pueden dar lugar a un árbol
\textit{infinito}\footnotemark.  \footnotetext{Podemos hablar de infinito
gracias a la evaluación lazy.}

Particularmente al desarrollar programas paralelos en Haskell, es muy común que
se compartan la mayoría de las computaciones. Por ejemplo, dentro de una
expresión como \haskell{par p q} el objetivo de generar un \emph{spark} para
la evaluación de \haskell{p} es que su valor es necesario en \haskell{q}, donde
en el caso que se pueda evaluar en paralelo, el programa se ejecutará más rápido.

Andy Gill presenta~\parencite{Gill:2009:TOS:1596638.1596653} un mecanismo de detección de computaciones compartidas,
utilizando funciones dentro de la mónada IO de Haskell y funciones de
tipo~\parencite{Chakravarty:2005:ATS:1090189.1086397}. Esta solución nos permite
construir el árbol abstracto utilizando primitivas
de nuestro EDSL y luego generar el grafo que representa nuestra computación, y
por lo tanto nos permite observar las computaciones
compartidas.

% En dicha solución introduce la función \haskell{reifyGraph} (de tipo como se muestra a continuación)
% que, dado un elemento \haskell{x} de tipo \haskell{t} construye un grafo de tipo \haskell{Graph},
% el cual representa al elemento \haskell{x} detectando las computaciones compartidas.
% La clase \haskell{MuRef} provee las operaciones necesarias para poder observar la estructura de
% un tipo dado, y para construir grafos con las computaciones compartidas. Para definir una instancia
% es necesario dar una función de tipo \haskell{DeRef}, que actúa como un sinónimo,
% entre el tipo a instanciar y su representación como un grafo,
% y una función \haskell{mapDeRef} que permite recorrer el árbol y transformar sus elementos en el
% nodo que los representa dentro del grafo final.
% El primer argumento de \haskell{mapDeRef} es una función que es aplicada a los
% hijos del segundo argumento, el cual es el nodo que se está analizando.
% El resultado es un nodo que contiene valores únicos de tipo \haskell{u}
% que fueron generados en base al primer argumento. La función \haskell{mapDeRef} utiliza
% funtores aplicativos~\parencite{mcbride08:applicative-programming} para proveer
% los efectos necesarios para la correcta asignación de los valores únicos.
%
% A continuación mostramos como definir la representación de un árbol de tipo \haskell{TPar},
% mediante el tipo \haskell{Node}.
% Notar que en el caso de la transformación
% de \haskell{Val'} (\haskell{TPar}) a \haskell{Val}(\haskell{Node}) dado en la definición
% de \haskell{mapDeRef}, el valor es ignorado,
% ya que no es necesario conservar el elemento
% debido a que se lo representa y trata como una caja cerrada.
% Utilizando la definición de \haskell{MuRef (TPar s)},
% permite detectar las computaciones
% compartidas una vez obtenido un valor de tipo \haskell{TPar s}, permitiendo al usuario
% construir su programa normalmente (sin obligar el uso de mónadas), y luego aplicar la función
% \haskell{reifyGraph}, ahora sí dentro de la mónada \haskell{IO}.
%
% Por ejemplo en el caso de la suma paralela (Código~\ref{lst:parsuma:cap4}), el resultado de aplicar la función
% \haskell{reifyGraph} es:
% \begin{lstlisting}
% reifyGraph (parsuma 4 7) =
%     let 
%         [(1,Par 2 3),(3,Seq 4 5),(5,Val ("(+) 4 7")),(4,Val ("7")),(2,Val ("4"))]
%     in 1
% \end{lstlisting}
% El resultado es una lista de pares compuestos por el valor único que representa al nodo, y el constructor
% correspondiente, y como resultado el valor único de la raíz del árbol. A modo descriptivo se introducen
% etiquetas que no están representadas en el constructor dado anteriormente.

Existen otras soluciones al problema de detectar computaciones compartidas. A
continuación detallamos algunas de estas y explicamos por qué no fueron
utilizadas en este trabajo.
\begin{itemize}
        \item Extensión a los lenguajes funcionales que permite observar (y por
                ende recorrer) este tipos de
                estructuras~\parencite{Claessen99observablesharing}.
                La solución consiste en poder definir un tipo de datos
                \haskell{Ref}, con tres operaciones. Permitir la creación de
                referencias a partir de un elemento, poder obtener el elemento a
                través de su referencia, y poder comparar referencias.
                Es una solución no conservativa, la cual implica tener que
                modificar el lenguaje anfitrión, en este caso Haskell.
        \item Etiquetado explícito, etiquetar a los nodos del árbol con un
                elemento único. En este caso, el usuario es el que provee las
                etiquetas manualmente.
        \item Las mónadas pueden ser utilizadas para generar las etiquetas de
                manera implícita, o bien, utilizar funtores aplicativos.  El
                problema de esta solución es que impacta directamente en el tipo
                de las primitivas básicas de la herramienta, obligando al
                usuario a utilizar un modelo monádico, obscureciendo el código.
                Es posible utilizar llamadas inseguras a la mónada \haskell{IO},
                y utilizar, lo que en programación imperativa se conoce como un
                contador, rompiendo las garantías que Haskell provee.
\end{itemize}

La solución elegida en el presente trabajo no exhibe los problemas descriptos.
Permite al usuario
de \nombre{} el desarrollo libre de efectos monádicos generados por la detección
de computaciones compartidas, sin tener que modificar el EDSL presentado en este
trabajo, ni Haskell.
